# Snowtools repository
[https://github.com/UMR-CNRM/snowtools](https://github.com/UMR-CNRM/snowtools)

## file I/O 
with Crocus/Surfex netcdf files into numpy arrays: [https://github.com/UMR-CNRM/snowtools/blob/master/snowtools/utils/prosimu.py](https://github.com/UMR-CNRM/snowtools/blob/master/snowtools/utils/prosimu.py) (see module-level docstring for example demo)